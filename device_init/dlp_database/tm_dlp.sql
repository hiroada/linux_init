/*
Navicat MySQL Data Transfer

Source Server         : 149
Source Server Version : 80030
Source Host           : 192.168.52.149:3306
Source Database       : tm_dlp

Target Server Type    : MYSQL
Target Server Version : 80030
File Encoding         : 65001

Date: 2023-07-13 08:57:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for d_config
-- ----------------------------
DROP TABLE IF EXISTS `d_config`;
CREATE TABLE `d_config` (
  `CONFIG_KEY` varchar(256) NOT NULL,
  `CONFIG_VAL` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for p_policy
-- ----------------------------
DROP TABLE IF EXISTS `p_policy`;
CREATE TABLE `p_policy` (
  `p_id` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0',
  `p_type` int DEFAULT NULL,
  `p_ver` int DEFAULT NULL,
  `p_ect` tinyint(1) DEFAULT NULL,
  `p_sts` tinyint(1) DEFAULT NULL,
  `p_content` mediumtext CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci,
  PRIMARY KEY (`p_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for t_alarm_code
-- ----------------------------
DROP TABLE IF EXISTS `t_alarm_code`;
CREATE TABLE `t_alarm_code` (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `CODE_KEY` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for t_lockinfo
-- ----------------------------
DROP TABLE IF EXISTS `t_lockinfo`;
CREATE TABLE `t_lockinfo` (
  `center_ip` varchar(50) DEFAULT NULL,
  `lock_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for t_quickscantask
-- ----------------------------
DROP TABLE IF EXISTS `t_quickscantask`;
CREATE TABLE `t_quickscantask` (
  `jobId` varchar(10) NOT NULL DEFAULT '',
  `ssid` varchar(100) DEFAULT NULL,
  `jobGroup` varchar(10) DEFAULT NULL,
  `jobStatus` varchar(2) DEFAULT NULL,
  `incrementalScan` int DEFAULT NULL,
  `cyc` varchar(2) DEFAULT NULL,
  `cronExpression` varchar(20) DEFAULT NULL,
  `dealType` varchar(2) DEFAULT NULL,
  `dealUrl` varchar(50) DEFAULT NULL,
  `dealUser` varchar(20) DEFAULT NULL,
  `dealPwd` varchar(20) DEFAULT NULL,
  `cot` text,
  `cod` text,
  `lim` text,
  `scanDetail` text,
  `runningState` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`jobId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for t_scantask
-- ----------------------------
DROP TABLE IF EXISTS `t_scantask`;
CREATE TABLE `t_scantask` (
  `jobId` varchar(10) NOT NULL DEFAULT '',
  `ssid` varchar(100) DEFAULT NULL,
  `jobGroup` varchar(10) DEFAULT NULL,
  `jobStatus` varchar(2) DEFAULT NULL,
  `incrementalScan` int DEFAULT NULL,
  `cyc` varchar(2) DEFAULT NULL,
  `cronExpression` varchar(20) DEFAULT NULL,
  `dealType` varchar(2) DEFAULT NULL,
  `dealUrl` varchar(50) DEFAULT NULL,
  `dealUser` varchar(20) DEFAULT NULL,
  `dealPwd` varchar(20) DEFAULT NULL,
  `cot` text,
  `cod` text,
  `lim` text,
  `scanDetail` text,
  `runningState` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`jobId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for t_stat
-- ----------------------------
DROP TABLE IF EXISTS `t_stat`;
CREATE TABLE `t_stat` (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_id` varchar(10) NOT NULL,
  `tatal` int NOT NULL,
  `current_number` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for t_white_list
-- ----------------------------
DROP TABLE IF EXISTS `t_white_list`;
CREATE TABLE `t_white_list` (
  `id` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0',
  `ver` int DEFAULT '0',
  `mtype` tinyint(1) DEFAULT '1',
  `content` mediumtext CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;
