#!/bin/bash
source /etc/profile
# 如果命令执行失败则立即退出
set -e
# 设置docker可以从http拉取
configure_docker() {
    # 设置 Docker 配置文件路径
    DAEMON_CONFIG="/etc/docker/daemon.json"

    # 检查 Docker 配置文件是否存在
    if [ ! -f "$DAEMON_CONFIG" ]; then
        echo "{}" > "$DAEMON_CONFIG"
    fi

    # 检查是否已配置 insecure-registries
    if ! grep -q '"insecure-registries"' "$DAEMON_CONFIG"; then
        # 需要添加 insecure-registries 配置
        # 使用 jq 工具来编辑 JSON 文件
        sudo yum install jq -y  # 在 CentOS 中使用 yum 安装 jq

        # 向 JSON 文件添加 insecure-registries
        TMP=$(mktemp)
        jq '. + {"insecure-registries":["0.0.0.0/0"]}' "$DAEMON_CONFIG" > "$TMP" && mv "$TMP" "$DAEMON_CONFIG"
        
        # 重启 Docker 服务应用更改
        sudo systemctl restart docker
        echo "Docker daemon configuration updated and service restarted."
    else
        echo "Insecure-registries already configured."
    fi
}

# Docker 安装脚本
# 检查 Docker 是否已安装
if command -v docker >/dev/null 2>&1; then
    echo "Docker 已经安装，退出脚本。"
    configure_docker
    exit 0
fi
yum install docker-26.0.0-1.x86_64 -y
# # 设置 Docker 版本号
# DOCKER_VERSION="26.0.0"

# # 检查特定目录下是否存在文件
# if [ -f "../../downloads/docker-${DOCKER_VERSION}.tgz" ]; then
#   echo "从 ../../downloads 中找到文件，正在复制..."
#   cp "../../downloads/docker-${DOCKER_VERSION}.tgz" "./docker.tgz"
#   echo "文件复制成功。"
# else
#   echo "在 ../../downloads 中未找到文件，尝试从 Gitee 下载..."
#   # 从 Gitee 下载，注意 Gitee 的链接是不直接支持原始文件下载的，这里需要正确的链接
#   wget --no-check-certificate "https://gitee.com/hiroada/downloads/raw/master/docker-${DOCKER_VERSION}.tgz" -O docker.tgz

#   # 检查 wget 命令是否成功执行
#   if [ $? -ne 0 ]; then
#     echo "从 Gitee 下载失败，尝试从 Docker 官方网站下载..."
#     # 从 Docker 官方网站下载
#     wget --no-check-certificate "https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz" -O docker.tgz
#     if [ $? -ne 0 ]; then
#       echo "从 Docker 官方网站下载失败，请检查网络连接或链接地址。"
#       exit 1
#     else
#       echo "文件成功下载自 Docker 官方网站。"
#     fi
#   else
#     echo "文件成功下载自 Gitee。"
#   fi
# fi

# echo "正在解压 Docker 包..."
# tar xvf docker.tgz

# # 检查上一个命令的退出状态
# if [ $? -ne 0 ]; then
#     echo "解压失败，退出脚本。"
#     exit 1
# fi

# # 将 Docker 二进制文件移动到 /usr/bin 目录
# echo "正在将 Docker 二进制文件移动到 /usr/bin/ 目录..."
# cp docker/* /usr/bin/

# # 删除已解压的目录和 tar 文件
# rm -rf docker docker.tgz

# 创建 Docker systemd 服务文件
echo "正在为 Docker 设置 systemd 服务..."
cat <<EOF > /etc/systemd/system/docker.service
[Unit]
Description=Docker 应用容器引擎
Documentation=https://docs.docker.com
After=network-online.target firewalld.service
Wants=network-online.target

[Service]
Type=notify
ExecStart=/usr/bin/dockerd
ExecReload=/bin/kill -s HUP \$MAINPID
LimitNOFILE=infinity
LimitNPROC=infinity
TimeoutStartSec=0
Delegate=yes
KillMode=process
Restart=on-failure
StartLimitBurst=3
StartLimitInterval=60s

[Install]
WantedBy=multi-user.target
EOF

# 为 Docker 服务文件添加执行权限
echo "为 Docker 服务添加执行权限..."
chmod +x /etc/systemd/system/docker.service

# 创建 Docker 配置目录和文件
echo "正在设置 Docker 守护进程配置..."
mkdir -p /etc/docker/
touch /etc/docker/daemon.json

# 向 /etc/docker/daemon.json 写入配置内容
cat <<EOF > /etc/docker/daemon.json
{
  "data-root": "/data/docker-root"
}
EOF

# 重新加载 systemd 守护进程配置
echo "正在重新加载 systemd 守护进程配置..."
systemctl daemon-reload

# 启动 Docker 服务
echo "正在启动 Docker 服务..."
systemctl start docker

# 设置 Docker 开机自启
echo "正在设置 Docker 开机自启..."
systemctl enable docker

echo "Docker 安装和设置完成。"
configure_docker


