#!/bin/bash

# 定义 GRUB 配置文件路径
GRUB_CONFIG_FILE="/etc/default/grub"

# 检查 GRUB 配置文件是否存在
if [ ! -f "$GRUB_CONFIG_FILE" ]; then
    echo "GRUB configuration file does not exist: $GRUB_CONFIG_FILE"
    exit 1
fi

# 检查 GRUB_CMDLINE_LINUX 中是否包含 isolcpus
if grep -q "GRUB_CMDLINE_LINUX" "$GRUB_CONFIG_FILE" && ! grep -q "isolcpus" "$GRUB_CONFIG_FILE"; then
    # 如果没有包含 isolcpus，追加 isolcpus=0-23
    sudo sed -i '/GRUB_CMDLINE_LINUX/s/"$/ isolcpus=16-23,80-87"/' "$GRUB_CONFIG_FILE"
    echo "isolcpus=16-23,80-87 has been appended to GRUB_CMDLINE_LINUX in $GRUB_CONFIG_FILE"
    
    # 提示用户生成新的 GRUB 配置文件
    echo "Please run 'sudo update-grub' to generate a new GRUB configuration file for the changes to take effect."
else
    echo "isolcpus setting is already present in GRUB_CMDLINE_LINUX or the line does not exist."
fi