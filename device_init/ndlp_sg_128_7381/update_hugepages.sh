#!/bin/bash

# # 路径到 GRUB 默认配置文件
# GRUB_CONFIG="/etc/default/grub"

# # 要添加的 hugepages 参数
# HUGEPAGES_PARAMS="default_hugepagesz=1G hugepagesz=1G hugepages=10"

# # 检查 GRUB_CMDLINE_LINUX 中是否已经有 hugepagesz 参数
# if grep -q "hugepagesz" "$GRUB_CONFIG"; then
#     echo "hugepagesz already set in GRUB_CMDLINE_LINUX."
# else
#     echo "Adding hugepagesz to GRUB_CMDLINE_LINUX."

#     # 在 GRUB_CMDLINE_LINUX 值中添加 hugepages 参数
#     # 使用 sed 来更新行
#     sudo sed -i "/^GRUB_CMDLINE_LINUX=/ s/\"$/ ${HUGEPAGES_PARAMS}\"/" "$GRUB_CONFIG"

#     # 更新 GRUB 配置
#     echo "GRUB configuration updated."
# fi