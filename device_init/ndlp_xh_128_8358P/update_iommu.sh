#!/bin/bash

# 定义GRUB配置文件路径
grub_config="/etc/default/grub"

# 设置IOMMU参数
iommu_cmd="intel_iommu=on"

# 备份GRUB配置文件
cp $grub_config ${grub_config}.bak

# 检查是否已经配置了IOMMU参数
if grep -q "$iommu_cmd" $grub_config; then
    echo "IOMMU is already configured in GRUB."
else
    # 更新GRUB配置文件中的GRUB_CMDLINE_LINUX参数
    if grep -q "^GRUB_CMDLINE_LINUX=" $grub_config; then
        sed -i "s/^GRUB_CMDLINE_LINUX=\"\(.*\)\"/GRUB_CMDLINE_LINUX=\"\1 $iommu_cmd\"/" $grub_config
    else
        echo "GRUB_CMDLINE_LINUX=\"$iommu_cmd\"" >> $grub_config
    fi
    echo "GRUB configuration has been updated. Please reboot your system."
fi