#!/bin/bash

# 需要设置的 file-max 值
EXPECTED_VALUE=1048576
FILE_MAX_CONFIG="fs.file-max = $EXPECTED_VALUE"

# 检查 /etc/sysctl.conf 是否包含 fs.file-max 配置行
FILE_MAX_LINE=$(grep -E "^fs\.file-max[[:space:]]*=" /etc/sysctl.conf)

# 检查 fs.file-max 设置是否存在并设置为期望值
if [[ $FILE_MAX_LINE ]]; then
    # fs.file-max 存在，提取当前的值
    CURRENT_VALUE=$(echo $FILE_MAX_LINE | cut -d '=' -f2 | tr -d ' ')
    
    if [[ "$CURRENT_VALUE" != "$EXPECTED_VALUE" ]]; then
        # 值不正确，使用 sed 进行替换
        sudo sed -i "s/^fs\.file-max[[:space:]]*=.*/$FILE_MAX_CONFIG/" /etc/sysctl.conf
        echo "fs.file-max updated to $EXPECTED_VALUE."
    else
        echo "fs.file-max is already set to $EXPECTED_VALUE."
    fi
else
    # fs.file-max 不存在，直接追加到文件末尾
    echo "$FILE_MAX_CONFIG" | sudo tee -a /etc/sysctl.conf > /dev/null
    echo "fs.file-max added with value $EXPECTED_VALUE."
fi

# 应用更改
sudo sysctl -p

# 确认更改已经生效
echo "Current fs.file-max value is: $(sysctl -n fs.file-max)"
