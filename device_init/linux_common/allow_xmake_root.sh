#!/bin/bash

# 检查 /etc/profile 是否已经有 XMAKE_ROOT 设置
if ! grep -q 'export XMAKE_ROOT=y' /etc/profile; then
    # 如果没有找到，追加这个设置到 /etc/profile
    echo 'export XMAKE_ROOT=y' | sudo tee -a /etc/profile > /dev/null

    # 输出结果并提示用户需要重启或重新加载 /etc/profile
    echo "XMAKE_ROOT has been added to /etc/profile. Please reboot or run 'source /etc/profile' to apply changes."
else
    # 如果已经添加过了，则不进行任何操作
    echo "XMAKE_ROOT is already set in /etc/profile."
fi
source /etc/profile
