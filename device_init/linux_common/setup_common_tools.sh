#!/bin/bash

# Ensure the script is run as root
if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
fi

# Update the package repository (optional, uncomment if needed)
# yum update -y

# Install OpenSSL and OpenSSH server
yum install -y openssl openssh-server

# Start and enable sshd service
systemctl start sshd.service
systemctl enable sshd.service

# Install additional utilities and tools
yum install -y vim-enhanced
yum install -y net-tools.x86_64
yum install -y wget
yum install -y tar
yum install -y xz
yum install -y unzip
yum install -y git
yum install -y sudo
yum install -y bc
yum install -y curl

# Install development tools
# yum install -y gcc gcc-devel
yum install -y gcc 
yum install -y gcc-c++ libstdc++-devel

# Install the library liblsan
yum install /usr/lib64/liblsan.so.0.0.0 -y

yum install mysql-community-client -y
yum install zlog -y
echo "All installations and configurations are complete."