#!/bin/bash

# Source the /etc/profile to load environment variables
source /etc/profile

# Function to create or update Docker proxy configuration
setup_proxy() {
    # Create Docker systemd drop-in directory if it doesn't exist
    sudo mkdir -p /etc/systemd/system/docker.service.d

    # Create or overwrite the http-proxy.conf file with the proxy settings
    sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf > /dev/null <<EOF
[Service]
Environment="HTTP_PROXY=$HTTP_PROXY"
Environment="HTTPS_PROXY=$HTTPS_PROXY"
Environment="NO_PROXY=localhost,127.0.0.1"
EOF

    # Reload systemd to apply changes
    sudo systemctl daemon-reload

    # Restart Docker service to apply proxy settings
    sudo systemctl restart docker

    echo "Docker proxy settings updated successfully with HTTP_PROXY and HTTPS_PROXY."
}

# Check if HTTP_PROXY and HTTPS_PROXY are set in the environment
if [ ! -z "$HTTP_PROXY" ] && [ ! -z "$HTTPS_PROXY" ]; then
    setup_proxy
else
    echo "HTTP_PROXY and HTTPS_PROXY are not set. No proxy configuration will be applied to Docker."
fi


# 定义MySQL数据、日志和配置文件的基本目录
MYSQL_DIR=/opt/mysql

# 检查是否传入了足够的参数
if [ "$#" -lt 3 ]; then
  echo "使用方法: $0 MYSQL_ROOT_PASSWORD REMOTE_USER REMOTE_PASSWORD [BIND_ADDRESS]"
  exit 1
fi
# 从命令行参数读取密码和用户名
MYSQL_ROOT_PASSWORD=$1
REMOTE_USER=$2
REMOTE_PASSWORD=$3
# 如果提供了第四个参数，则读取它作为绑定地址
BIND_ADDRESS=$4

# 确保Docker正在运行
if ! docker info >/dev/null 2>&1; then
    echo "Docker似乎没有运行,请先启动它然后重试"
    exit 1
fi

# # 检查是否已经拉取了MySQL镜像
IMAGE="mysql:8.0"
LOCAL_REGISTRY="192.168.52.44:5000/$IMAGE"



if docker load -i ../../downloads/mysql_8.0.tar; then  
    docker tag $LOCAL_REGISTRY $IMAGE
    echo "docker load mysql_8.0执行成功。"  
else  
    if ! docker image inspect $IMAGE >/dev/null 2>&1; then
        echo "MySQL 8.0镜像不存在，尝试从本地私有仓库拉取..."

        # 尝试从本地私有仓库拉取
        if docker pull $LOCAL_REGISTRY; then
            echo $LOCAL_REGISTRY
            echo "从本地私有仓库成功拉取到 MySQL 8.0 镜像。"
            docker tag $LOCAL_REGISTRY $IMAGE
            docker rmi $LOCAL_REGISTRY
        else
            echo "从本地私有仓库拉取失败，尝试从 Docker Hub 拉取..."
            
            # 尝试从 Docker Hub 拉取
            if docker pull $IMAGE; then
                echo "从 Docker Hub 成功拉取到 MySQL 8.0 镜像。"
            else
                echo "从 Docker Hub 拉取 MySQL 8.0 镜像失败。"
                exit 1
            fi
        fi
    else
        echo "MySQL 8.0镜像已经存在。"
    fi
fi

# 检查每个目录是否存在，如果不存在则创建
for dir in conf logs data; do
  if [ ! -d "${MYSQL_DIR}/${dir}" ]; then
    echo "正在创建目录 ${MYSQL_DIR}/${dir}..."
    mkdir -p "${MYSQL_DIR}/${dir}"
  else
    echo "目录 ${MYSQL_DIR}/${dir} 已经存在。"
  fi
done

# 检查是否有名为mysql的容器已经存在或正在运行
CONTAINER_NAME="mysql"
if [ "$(docker ps -aq -f name=^/${CONTAINER_NAME}$)" ]; then
    echo "一个名为 $CONTAINER_NAME 的容器已经存在。检查它是否正在运行..."
    if [ "$(docker ps -q -f name=^/${CONTAINER_NAME}$)" ]; then
        echo "MySQL容器已经在运行。"
    else
        echo "正在启动已存在的MySQL容器..."
        docker start $CONTAINER_NAME
    fi
else
    echo "正在启动MySQL容器..."
    # 根据是否提供了BIND_ADDRESS来决定启动参数
    if [ -z "$BIND_ADDRESS" ]; then
        # 使用host网络模式
        docker run --name $CONTAINER_NAME \
          --net=host \
          -v "${MYSQL_DIR}/conf:/etc/mysql/conf.d" \
          -v "${MYSQL_DIR}/logs:/var/log/mysql" \
          -v "${MYSQL_DIR}/data:/var/lib/mysql" \
          -e MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD \
          -d $IMAGE
    else
        # 使用指定的绑定地址和端口映射
        docker run --name $CONTAINER_NAME \
          -v "${MYSQL_DIR}/conf:/etc/mysql/conf.d" \
          -v "${MYSQL_DIR}/logs:/var/log/mysql" \
          -v "${MYSQL_DIR}/data:/var/lib/mysql" \
          -e MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD \
          -p $BIND_ADDRESS:3306 \
          -d $IMAGE
    fi
    echo "MySQL容器已经以 ${MYSQL_DIR} 为卷启动。"
fi

# 循环等待容器完全启动
while ! docker exec $CONTAINER_NAME mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "SELECT 1" >/dev/null 2>&1; do
    echo "等待容器内部MYSQL启动成功..."
    sleep 5
done

# 检查用户是否存在，并删除已存在的用户
echo "检查用户 $REMOTE_USER 是否存在..."
USER_EXISTS=$(docker exec $CONTAINER_NAME mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "SELECT 1 FROM mysql.user WHERE user = '$REMOTE_USER';")
if [[ "$USER_EXISTS" == *1* ]]; then
    echo "用户 $REMOTE_USER 已存在。正在删除用户..."
    docker exec $CONTAINER_NAME mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "DROP USER IF EXISTS '$REMOTE_USER'@'%';FLUSH PRIVILEGES;"
fi

# 创建允许远程访问的用户并指定身份验证插件为mysql_native_password
echo "创建用于远程访问的MySQL用户，并指定使用mysql_native_password插件..."
docker exec $CONTAINER_NAME mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "CREATE USER '$REMOTE_USER'@'%' IDENTIFIED WITH mysql_native_password BY '$REMOTE_PASSWORD';"
docker exec $CONTAINER_NAME mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "GRANT ALL PRIVILEGES ON *.* TO '$REMOTE_USER'@'%' WITH GRANT OPTION;"
docker exec $CONTAINER_NAME mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "FLUSH PRIVILEGES;"

echo "用于远程访问的MySQL用户 $REMOTE_USER 已创建。"