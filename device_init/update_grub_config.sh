#!/bin/bash

# 检查 EFI 目录是否存在
if [ -d "/sys/firmware/efi" ]; then
    echo "EFI directory exists. Configuring GRUB for EFI."
    sudo grub2-mkconfig -o /boot/efi/EFI/openEuler/grub.cfg || {
        echo "Failed to configure GRUB for EFI. Configuring GRUB for Legacy BIOS instead."
        sudo grub2-mkconfig -o /boot/grub2/grub.cfg
    }
else
    echo "EFI directory does not exist. Configuring GRUB for Legacy BIOS."
    sudo grub2-mkconfig -o /boot/grub2/grub.cfg
fi