#!/bin/bash

# 检查 watm_downloads 环境变量是否存在
if [ -z "$watm_downloads" ]; then
    echo "环境变量 watm_downloads 未设置。请先设置该变量。"
    exit 1
fi

# 定义文件和下载URL
FILE_NAME="ice-1.14.9.tar.gz"
DOWNLOAD_URL="https://downloadmirror.intel.com/822519/$FILE_NAME"
DEST_DIR="$HOME"

# 检查 watm_downloads 目录中是否存在 ice-1.14.9.tar.gz
if [ -f "$watm_downloads/$FILE_NAME" ]; then
    echo "文件 $FILE_NAME 已存在于 $watm_downloads 目录。"
else
    echo "文件 $FILE_NAME 不存在于 $watm_downloads 目录。正在下载..."
    
    # 下载文件到 watm_downloads 目录
    wget -P "$watm_downloads" "$DOWNLOAD_URL"
    
    if [ $? -ne 0 ]; then
        echo "文件下载失败。请检查 URL 和网络连接。"
        exit 1
    fi
fi

# 拷贝文件到用户主目录
cp "$watm_downloads/$FILE_NAME" "$DEST_DIR"

# 解压文件
tar -xzf "$DEST_DIR/$FILE_NAME" -C "$DEST_DIR"

# 进入解压目录
cd "$DEST_DIR/ice-1.14.9" || exit

# 进入 src 目录
cd src || exit

# 编译和安装
make && make install

if [ $? -ne 0 ]; then
    echo "编译或安装失败。请检查编译日志。"
    exit 1
fi

# 返回主目录并删除文件和目录
cd "$DEST_DIR"
rm -f "$FILE_NAME"
rm -rf "ice-1.14.9"

echo "操作完成。"