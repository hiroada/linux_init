#!/bin/bash
source /etc/profile
# 检查是否已安装 DPDK
if [ -f /usr/local/lib64/pkgconfig/libdpdk.pc ]; then
    echo "DPDK is already installed."
    exit 0
fi

# filename="dpdk-22.11.3.tar.xz"

local current_dir=$(pwd)


# # 检查 ../../downloads 目录下是否有文件
# if [ -f "../../downloads/$filename" ]; then
#     echo "从本地 ../../downloads 复制文件..."
#     cp "../../downloads/$filename" .
#     echo "$filename 复制成功。"
# else
#     echo "$filename 在本地不存在，尝试从 Gitee 下载..."
#     # 尝试从 Gitee 下载文件
#     wget --no-check-certificate "https://gitee.com/hiroada/downloads/raw/master/$filename" -O $filename

#     # 检查 wget 命令是否成功执行
#     if [ $? -ne 0 ]; then
#         echo "从 Gitee 下载失败，尝试从官方网站下载..."
#         # 尝试从官方网站下载文件
#         wget --no-check-certificate "https://fast.dpdk.org/rel/$filename" -O $filename
#         if [ $? -ne 0 ]; then
#             echo "从官方网站下载失败，请检查网络连接或链接地址。"
#             exit 1
#         else
#             echo "$filename 成功下载自官方网站。"
#         fi
#     else
#         echo "$filename 成功下载自 Gitee。"
#     fi
# fi

# yum install -y numactl-devel numactl-libs
# cp local.conf /etc/ld.so.conf.d/ -rf
# yum install kernel-devel-$(uname -r) -y

# pip3 install meson
# pip3 install pyelftools
# yum install ninja-build -y
# tar -xvf dpdk-22.11.3.tar.xz
# if [ $? -ne 0 ]; then  # 检查上一条命令的退出状态
#     echo "解压失败，即将退出脚本"
#     exit 1
# fi
# cd dpdk-stable-22.11.3/
# meson setup /usr/src/dpdk-stable-22.11.3
# cd /usr/src/dpdk-stable-22.11.3/
# meson configure -Dexamples=all
# ninja
# ninja install
# ldconfig

# yum install -y numactl-devel numactl-libs numactl
# yum install kernel-devel-$(uname -r) -y
# yum install meson -y
# yum install python3-pyelftools -y
# yum install ninja-build -y

cp local.conf /etc/ld.so.conf.d/ -rf
yum install dpdk-22.11.3-1.x86_64 -y
ldconfig
# 再次查找 libdpdk.pc 确认安装成功
echo "export PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig" >> /etc/profile
source /etc/profile

# 加载必要的内核模块
modprobe vfio-pci
modprobe uio_pci_generic
cd "$current_dir"
rm dpdk-22.11.3.tar.xz -rf
rm dpdk-stable-22.11.3/ -rf
