yum install -y openssl-devel
yum install -y libasan.x86_64
yum install -y perf
yum install -y libxml2-devel.x86_64
yum install -y libuuid-devel
yum install -y gcc-c++
yum install -y gdb
yum install -y make
yum install -y libpcap-devel
yum install -y python3
yum install -y tar
yum install -y openssl
yum install -y autoconf
yum install -y automake
yum install -y libtool
yum install -y flex
yum install -y byacc
yum install -y bison
yum install -y bison-devel
yum install -y net-tools
yum install -y cmake
yum install -y numactl
yum install -y numactl-devel
yum install -y numactl-libs
yum install -y pciutils
yum install -y iproute
yum install -y kmod
yum install -y python3-pyelftools
yum install kernel-devel-$(uname -r) -y

yum install -y maven
yum install -y jansson-devel
yum install -y libuuid-devel
yum install -y uchardet-devel
yum install zlog-1.2.16-1.x86_64
yum install -y mysql-community-common-8.0.34-1.el8.x86_64
yum install -y mysql-community-client-plugins-8.0.34-1.el8
yum install -y mysql-community-libs-8.0.34-1.el8.x86_64
yum install -y mysql-community-client-8.0.34-1.el8.x86_64
yum install -y mysql-community-icu-data-files-8.0.34-1.el8
#yum install -y mysql-community-server-8.0.34-1.el8.x86_64
yum install -y mysql-community-devel-8.0.34-1.el8.x86_64
cp /usr/lib64/mysql/libmysqlclient.so.21.2.34 /usr/lib64/libmysqlclient.so
yum install -y libzmq-4.3.5-1.x86_64
yum install -y libczmq-4.2.2-1.x86_64
yum install -y libiconv-1.17-1.x86_64