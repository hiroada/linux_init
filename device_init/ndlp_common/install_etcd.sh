#!/bin/bash

# 设置脚本在遇到错误时退出
set -e

# 定义下载的版本和配置
# ETCD_VERSION="v3.5.13"
# ETCD_DOWNLOAD_URL="https://github.com/etcd-io/etcd/releases/download/${ETCD_VERSION}/etcd-${ETCD_VERSION}-linux-amd64.tar.gz"
# ETCD_TAR_FILE="etcd-${ETCD_VERSION}-linux-amd64.tar.gz"

# # 步骤1: 检查etcd是否已安装
# if command -v etcd >/dev/null 2>&1; then
#     echo "etcd 已安装"
#     etcd --version
# else
#     # 如果未安装，进行下载和安装
#     if [ ! -f "$ETCD_TAR_FILE" ]; then
#         echo "正在下载 etcd ${ETCD_VERSION}..."
#         # 检查 ../../downloads 目录下是否有文件
#         if [ -f "../../downloads/$ETCD_TAR_FILE" ]; then
#             echo "从本地 ../../downloads 复制文件..."
#             cp "../../downloads/$ETCD_TAR_FILE" .
#             echo "$ETCD_TAR_FILE 复制成功。"
#         else
#             echo "$ETCD_TAR_FILE 在本地不存在，尝试从 Gitee 下载..."
#             # 尝试从 Gitee 下载文件
#             wget --no-check-certificate "https://gitee.com/hiroada/downloads/raw/master/$ETCD_TAR_FILE" -O $ETCD_TAR_FILE
#             if [ $? -ne 0 ]; then
#                 echo "从 Gitee 下载失败，尝试从原始 URL 下载..."
#                 # 尝试从原始 URL 下载文件
#                 wget "$ETCD_DOWNLOAD_URL" -O $ETCD_TAR_FILE || { echo "下载失败"; exit 1; }
#             else
#                 echo "$ETCD_TAR_FILE 成功下载自 Gitee。"
#             fi
#         fi
#     fi

#     echo "正在解压 etcd ${ETCD_VERSION}..."
#     tar -xvf ${ETCD_TAR_FILE} || { echo "解压失败"; exit 1; }

#     echo "正在安装 etcd 和 etcdctl..."
#     sudo mv etcd-${ETCD_VERSION}-linux-amd64/etcd /usr/local/bin/
#     sudo mv etcd-${ETCD_VERSION}-linux-amd64/etcdctl /usr/local/bin/
#     echo "etcd 安装完成。"
# fi

# 步骤1: 检查etcd是否已安装
if command -v etcd >/dev/null 2>&1; then
    echo "etcd 已安装"
    etcd --version
else
    # 如果未安装，进行下载和安装
    yum install -y etcd-v3.5.13-1.x86_64
    if [ $? -eq 0 ]; then  
        echo "etcd 安装成功"  
    else  
        echo "安装etcd失败，返回值为 $?"
        exit 1
    fi
fi

# 创建配置目录和文件
ETCD_SERVICE_FILE="/etc/systemd/system/etcd.service"
sudo mkdir -p /var/lib/etcd
sudo mkdir -p /etc/etcd

# 创建 systemd 服务文件
echo "[Unit]
Description=etcd service
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/etcd --listen-client-urls http://127.0.0.1:2379 --advertise-client-urls http://127.0.0.1:2379
Restart=on-failure
User=root

[Install]
WantedBy=multi-user.target" | sudo tee ${ETCD_SERVICE_FILE}

# 重新加载系统服务配置, 启动 etcd 服务
sudo systemctl daemon-reload
sudo systemctl start etcd

# 检查 etcd 服务是否已设置为开机自启
if sudo systemctl is-enabled etcd | grep -q "enabled"; then
    echo "etcd 已设置为开机自启动。"
else
    echo "etcd 未设置为开机自启动，现在设置中..."
    sudo systemctl enable etcd
    echo "etcd 现已设置为开机自启动。"
fi

echo "etcd 配置为每次开机启动，并且只监听 127.0.0.1。"