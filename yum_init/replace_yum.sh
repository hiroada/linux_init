#!/bin/bash  
  
# 检查是否提供了参数  
if [ -z "$1" ]; then  
    echo "Usage: $0 {remote|local}"  
    exit 1  
fi  
  
# 定义一些变量  
REMOTE_REPO_FILE="./openEulerSelf.repo"  
LOCAL_REPO_FILE="./local.repo"  
DOWNLOADS_DIR="$(realpath "$(dirname "$0")/../downloads")"
REPO_DATA_DIR="$DOWNLOADS_DIR/repodata" # 假设仓库数据放在这里  
REPO_FILE=""

# 检查下载目录是否存在  
if [ ! -d "$DOWNLOADS_DIR" ]; then  
    echo "Error: The downloads directory does not exist: $DOWNLOADS_DIR"  
    exit 1  
fi  
  
# 根据参数执行不同的操作  
case "$1" in  
    remote)  
        # 替换openEulerSelf.repo（这里假设它已存在且格式正确）  
        # 注意：这里没有实际的替换逻辑，因为通常需要更复杂的逻辑来下载或更新远程repo  
        # 备份原有的repo文件（可选，但推荐）  
        REPO_FILE=$REMOTE_REPO_FILE
        echo "Replacing remote repo file $REMOTE_REPO_FILE (not implemented)"  
        ;;  
    local)  
        # 获取../downloads的绝对路径，并更新local.repo中的baseurl  
        # 假设local.repo中有一个固定的行来设置baseurl，我们使用sed来替换它  
        # 注意：这里的行数（例如，第3行）可能需要根据你的local.repo文件的实际内容来调整  
        sed -i "3s|.*|baseurl=file://$REPO_DATA_DIR|g" "$LOCAL_REPO_FILE"  

        rpm -ivh "$REPO_DATA_DIR/drpm-*"
        rpm -ivh "$REPO_DATA_DIR/createrepo_c-*"
        createrepo "$REPO_DATA_DIR"
        REPO_FILE=$LOCAL_REPO_FILE
        ;;  
    *)  
        echo "Invalid option: $1"  
        exit 1  
        ;;  
esac  

BACKUP_DIR="/etc/yum.repos.d.backup/$(date +%Y%m%d%H%M%S)"
mkdir -p "$BACKUP_DIR"
mv /etc/yum.repos.d/*.repo "$BACKUP_DIR"
\cp "$REPO_FILE" "/etc/yum.repos.d/"
# 使新的repo配置生效（对于所有情况）  
if command -v dnf &> /dev/null; then  
    dnf clean all  
    dnf makecache  
else  
    yum clean all  
    yum makecache  
fi  
  
echo "Repo has been updated and cache has been refreshed."