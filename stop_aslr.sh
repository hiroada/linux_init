#!/bin/bash

# 禁用 ASLR
sudo sysctl -w kernel.randomize_va_space=0

# 检查 /etc/sysctl.conf 中是否已经包含该设置
grep -q "^kernel.randomize_va_space" /etc/sysctl.conf

if [ $? -eq 0 ]; then
    # 如果已经包含，则更新该设置
    sudo sed -i 's/^kernel.randomize_va_space.*/kernel.randomize_va_space = 0/' /etc/sysctl.conf
else
    # 如果不包含，则添加该设置
    echo "kernel.randomize_va_space = 0" | sudo tee -a /etc/sysctl.conf
fi

# 应用配置
sudo sysctl -p

echo "ASLR 已禁用，并配置为在重启后仍然有效。"