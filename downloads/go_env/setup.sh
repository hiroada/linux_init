#!/bin/sh
mkdir /work
mkdir /work/go
mkdir /work/go/GOPATH
mkdir /work/go/IDE
mkdir /work/go/workspace
rm -rf /usr/local/go
tar -C /usr/local -xzf go1.23.3.linux-amd64.tar.gz
chmod -R 777 /work

cat env >> /etc/profile
cat env >> ~/.bashrc
source /etc/profile
go install -v github.com/go-delve/delve/cmd/dlv@latest
go install -v golang.org/x/tools/gopls@latest