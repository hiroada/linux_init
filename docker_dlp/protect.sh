#!/bin/bash
source /etc/profile
#####################装载目录####################
# 定义目录路径
DIR="/dev/shm/session_json"

# 检查目录是否存在
if [ ! -d "$DIR" ]; then
    # 如果目录不存在，创建目录
    mkdir "$DIR"
    
    # 检查目录是否创建成功
    if [ $? -eq 0 ]; then
        echo "Directory $DIR has been created successfully."
    else
        echo "Failed to create directory $DIR." >&2
        exit 1
    fi
else
    echo "Directory $DIR already exists."
fi

# 设置权限，使得所有用户都可以访问
chmod 777 "$DIR"

# 通知用户权限已设置
echo "Access permissions for $DIR have been set for all users."


#####################启动守护进程####################

# cd /opt/TM_DLP/dlp_protect
# # 循环等待容器完全启动
# while ! mysql -h 127.0.0.1 -uwatm -pWAtm@951.mysql -e "SELECT 1" >/dev/null 2>&1; do
#     echo "等待MYSQL容器启动成功..."
#     sleep 2
# done
# nohup java -jar protect.jar > /dev/null 2>&1 &

cd /opt/TM_DLP/dlp_manage
# 循环等待容器完全启动
while ! mysql -h 127.0.0.1 -uwatm -pWAtm@951.mysql -e "SELECT 1" >/dev/null 2>&1; do
    echo "等待MYSQL容器启动成功..."
    sleep 2
done
nohup java -Dlogging.config=classpath:logback-protect.xml -jar AllInOne.jar --spring.profiles.active=protect,commonProtect > /dev/null 2>&1 &