#!/bin/bash

# 定义 gai.conf 文件的路径
GAI_CONF="/etc/gai.conf"

# 检查 gai.conf 文件是否存在
if [ ! -f "$GAI_CONF" ]; then
    echo "gai.conf 文件不存在。"
    # 检查示例文件是否存在
    if [ -f "/etc/gai.conf.sample" ]; then
        echo "从示例文件复制 gai.conf..."
        sudo cp "/etc/gai.conf.sample" "$GAI_CONF"
    else
        echo "示例配置文件也不存在，正在创建新的 gai.conf 文件..."
        # 创建新的 gai.conf 文件并设置默认配置
        echo "# This file is created by script to set IPv4 priority over IPv6" | sudo tee "$GAI_CONF"
        echo "precedence ::ffff:0:0/96  100" | sudo tee -a "$GAI_CONF"
    fi
fi

# 备份原始的 gai.conf 文件
BACKUP_PATH="$GAI_CONF.backup"
if [ ! -f "$BACKUP_PATH" ]; then
    echo "正在备份原始 gai.conf 文件到 $BACKUP_PATH..."
    sudo cp "$GAI_CONF" "$BACKUP_PATH"
else
    echo "备份文件 $BACKUP_PATH 已存在，跳过备份步骤。"
fi

# 确保 gai.conf 文件中设置了IPv4优先
if ! grep -q "precedence ::ffff:0:0/96  100" "$GAI_CONF"; then
    echo "添加 IPv4 优先配置到 gai.conf..."
    echo "precedence ::ffff:0:0/96  100" | sudo tee -a "$GAI_CONF"
else
    echo "IPv4 优先配置已存在于 gai.conf 中。"
fi

# 提示用户可能需要重启网络服务或机器
echo "配置完成。请考虑重启网络服务或重启服务器以应用更改。"