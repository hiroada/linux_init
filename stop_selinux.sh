#!/bin/bash

# 检查是否以root用户运行
if [ "$EUID" -ne 0 ]; then
  echo "请以root用户运行此脚本。"
  exit 1
fi

# 禁用当前会话中的SELinux
setenforce 0

# 修改SELinux配置文件以禁用SELinux
if [ -f /etc/selinux/config ]; then
  sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
  echo "SELinux 已被禁用，并将在下次重启后保持禁用状态。"
else
  echo "找不到SELinux配置文件 /etc/selinux/config。"
  exit 1
fi

# 确认当前状态，确认关闭
sestatus