#!/bin/sh

# 获取主板信息
device_name=$(sudo dmidecode -t baseboard | grep "Product Name" | cut -d: -f2 | tr -d ' ')
cpu_model=$(lscpu | grep "Model name" | awk -F: '{print $2}' | sed 's/^ *//g' | head -n 1)
# 定义一个函数来运行目录下的所有脚本
run_all_scripts_in_directory() {
    for script in "$1"/*; do
        if [ -x "$script" ] && [ ! -d "$script" ]; then
            "$script"
        fi
    done
}

# 检查产品名称是否匹配
if [ "$device_name" = "SC612DI-16F" ]; then
    # 运行当前目录下 dlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./dlp_common
    # 运行当前目录下 ndlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./ndlp_common
    # 运行当前目录下 ndlp_start 目录下的所有脚本
    run_all_scripts_in_directory ./ndlp_start
fi

# 检查产品名称是否匹配
if [ "$device_name" = "62DB32" ]; then
    # 运行当前目录下 dlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./dlp_common
    # 运行当前目录下 ndlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./ndlp_common
    # 运行当前目录下 ndlp_start 目录下的所有脚本
    run_all_scripts_in_directory ./ndlp_start
fi

# 检查产品名称是否匹配
if [ "$device_name" = "52DB16" ]; then
    # 运行当前目录下 dlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./dlp_common
    # 运行当前目录下 ndlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./ndlp_common
    # 运行当前目录下 ndlp_start 目录下的所有脚本
    run_all_scripts_in_directory ./ndlp_start
fi