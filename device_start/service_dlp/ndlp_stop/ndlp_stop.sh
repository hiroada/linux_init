#!/bin/bash

# 函数用于杀死特定的 jar 进程
kill_jar_process() {
  local jar_name=$1
  # 使用 pgrep 获取对应 jar 文件的进程 ID (PID)
  local pids=$(pgrep -f "$jar_name")
  if [ ! -z "$pids" ]; then
    echo "Killing $jar_name processes with PIDs: $pids"
    kill $pids
    # 等待进程结束
    for pid in $pids; do
      wait $pid 2>/dev/null
    done
    echo "$jar_name processes have been terminated."
  else
    echo "No process found for $jar_name."
  fi
}

# 函数用于杀死所有 ./ndlp 进程
kill_ndlp_processes() {
  # 使用 pgrep 获取所有 ./ndlp 相关的进程 ID (PID)
  local pids=$(pgrep -f './ndlp ./conf/conf_flows')
  if [ ! -z "$pids" ]; then
    echo "Killing ./ndlp processes with PIDs: $pids"
    kill -9 $pids
    # 等待进程结束
    for pid in $pids; do
      wait $pid 2>/dev/null
    done
    echo "./ndlp processes have been terminated."
  else
    echo "No process found for ./ndlp."
  fi
}

# 函数用于杀死所有 ./audit 进程
kill_audit_processes() {
  # 使用 pgrep 获取所有 ./audit 相关的进程 ID (PID)
  local pids=$(pgrep -f './audit ./conf/conf_audit_micro.json')
  if [ ! -z "$pids" ]; then
    echo "Killing ./audit processes with PIDs: $pids"
    kill -9 $pids
    # 等待进程结束
    for pid in $pids; do
      wait $pid 2>/dev/null
    done
    echo "./audit processes have been terminated."
  else
    echo "No process found for ./audit."
  fi
}

# 依次杀死 jar 进程，首先是 protect.jar
echo "Checking for AllInOne.jar process..."
kill_jar_process "AllInOne.jar"

# # 依次杀死 jar 进程，首先是 protect.jar
# echo "Checking for protect.jar process..."
# kill_jar_process "protect.jar"

# echo "Checking for daemon.jar process..."
# kill_jar_process "daemon.jar"

# echo "Checking for incident.jar process..."
# kill_jar_process "incident.jar"

# 然后杀死所有 ./ndlp 进程
echo "Checking for ./ndlp processes..."
kill_ndlp_processes

# 然后杀死所有 ./audit 进程
echo "Checking for ./audit processes..."
kill_audit_processes

echo "All specified processes have been checked and killed if they were running."