#!/bin/bash
configure_hugepages() {
    local busid="$1"
    local pagenum="$2"
    local numa_node_file="/sys/bus/pci/devices/$busid/numa_node"
    local numa_node

    # 检查numa_node文件是否存在
    if [ ! -f "$numa_node_file" ]; then
        echo "Error: The file $numa_node_file does not exist. Check the busid."
        exit 1
    fi

    numa_node=$(cat "$numa_node_file")
    local hugepages_file="/sys/devices/system/node/node$numa_node/hugepages/hugepages-1048576kB/nr_hugepages"

    # 检查hugepages文件是否存在
    if [ ! -f "$hugepages_file" ]; then
        echo "Error: The file $hugepages_file does not exist. Check the busid."
        exit 1
    fi

    # 设置hugepages的数量
    echo "$pagenum" > "$hugepages_file"

    # 创建挂载点并挂载hugetlbfs
    # mkdir -p /mnt/huge_1GB
    # mount -t hugetlbfs -o pagesize=1GB nodev /mnt/huge_1GB
}

# 获取主板信息
device_name=$(sudo dmidecode -t baseboard | grep "Product Name" | cut -d: -f2 | tr -d ' ')
cpu_model=$(lscpu | grep "Model name" | awk -F: '{print $2}' | sed 's/^ *//g' | head -n 1)
# 加载必要的内核模块
modprobe vfio-pci
modprobe uio_pci_generic

# 宝德旧设备32核
if [ "$device_name" = "SC612DI-16F" ]; then
    dpdk-devbind.py -b uio_pci_generic 0000:02:00.0
    dpdk-devbind.py -b uio_pci_generic 0000:02:00.1
    configure_hugepages "0000:02:00.0" 5
fi

# 曙光设备
if [ "$device_name" = "62DB32" ]; then
    # 曙光设备128核项目46台设备
    if [ "$cpu_model" = "Hygon C86 7390 32-core Processor" ]; then
        dpdk-devbind.py -b uio_pci_generic 0000:e1:00.0
        dpdk-devbind.py -b uio_pci_generic 0000:e1:00.1
        configure_hugepages "0000:e1:00.0" 20
        echo 20 > /sys/devices/system/node/node0/hugepages/hugepages-1048576kB/nr_hugepages
        mkdir -p /mnt/huge_1GB
        mount -t hugetlbfs -o pagesize=1GB nodev /mnt/huge_1GB
    fi
    # 曙光设备128核
    if [ "$cpu_model" = "Hygon C86 7381 32-core Processor" ]; then
        dpdk-devbind.py -b uio_pci_generic 0000:a1:00.0
        dpdk-devbind.py -b uio_pci_generic 0000:a1:00.1
        configure_hugepages "0000:a1:00.0" 10
    fi
fi

# XH设备
if [ "$device_name" = "BC13MBSBH" ]; then
    # XH64原材料设备

    if [ "$cpu_model" = "Intel(R) Xeon(R) Silver 4314 CPU @ 2.40GHz" ]; then
        dpdk-devbind.py -b uio_pci_generic 0000:4c:00.0
        dpdk-devbind.py -b uio_pci_generic 0000:4c:00.1
        configure_hugepages "0000:4c:00.0" 5
        echo 128 > /sys/devices/system/node/node1/hugepages/hugepages-2048kB/nr_hugepages
    fi
    # XH128原材料设备
    if [ "$cpu_model" = "Intel(R) Xeon(R) Platinum 8358P CPU @ 2.60GHz" ]; then
        dpdk-devbind.py -b igb_uio 0000:4c:00.0
        dpdk-devbind.py -b igb_uio 0000:4c:00.1
        configure_hugepages "0000:4c:00.0" 8
        echo 128 > /sys/devices/system/node/node1/hugepages/hugepages-2048kB/nr_hugepages
    fi
fi

# 曙光设备64核
if [ "$device_name" = "52DB16" ]; then
    dpdk-devbind.py -b uio_pci_generic 0000:61:00.0
    dpdk-devbind.py -b uio_pci_generic 0000:61:00.1
    configure_hugepages "0000:61:00.0" 5
fi

# 安全检查设备96核
if [ "$device_name" = "X11DPH-i" ]; then
    dpdk-devbind.py -b igb_uio 0000:1a:00.1
    configure_hugepages "0000:1a:00.1" 5
    echo 128 > /sys/devices/system/node/node1/hugepages/hugepages-2048kB/nr_hugepages
    echo 5 > /sys/devices/system/node/node0/hugepages/hugepages-1048576kB/nr_hugepages
fi

mkdir -p /mnt/huge_1GB
mount -t hugetlbfs -o pagesize=1GB nodev /mnt/huge_1GB

