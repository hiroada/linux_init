#!/bin/bash

# 定义默认仓库地址和默认镜像名称
DEFAULT_REPOSITORIES=("192.168.52.44:5000" "203.90.130.203:5000")
DEFAULT_IMAGE_NAME="tm_dlp"
IMAGE_NAME=$DEFAULT_IMAGE_NAME  # 初始设为默认镜像名

# 检查容器tm_dlp是否存在
container_exists=$(docker ps -a --format '{{.Names}}' | grep -w "tm_dlp")
if [ -z "$container_exists" ]; then
    echo "容器tm_dlp不存在，尝试获取最新镜像。"
    latest_image=""
    latest_time=""

    # 根据参数数量选择逻辑
    if [ $# -eq 0 ]; then
        # 没有参数，遍历每个默认仓库查找最新镜像
        for repo in "${DEFAULT_REPOSITORIES[@]}"; do
            tags=$(curl -s "http://$repo/v2/$IMAGE_NAME/tags/list" | jq -r '.tags[]')
            for tag in $tags; do
                manifest=$(curl -s -H "Accept: application/vnd.docker.distribution.manifest.v2+json" "http://$repo/v2/$IMAGE_NAME/manifests/$tag")
                created_time=$(echo $manifest | jq -r '.history[0].v1Compatibility' | jq -r '.created')
                if [[ "$created_time" > "$latest_time" ]]; then
                    latest_time=$created_time
                    latest_image="$repo/$IMAGE_NAME:$tag"
                fi
            done
        done
    elif [ $# -eq 1 ]; then
        # 有一个参数，假定为完整的 name:tag 形式
        if [[ $1 =~ ^.+/.+:.+$ ]]; then
            full_image=$1
            latest_image=$full_image
            echo "从指定的镜像 $full_image 拉取。"
        else
            echo "错误的参数格式。正确的调用格式: ./docker_manage.sh [repository/image:tag]"
            exit 1
        fi
    else
        echo "脚本参数错误。正确的调用格式: ./docker_manage.sh [repository/image:tag]"
        exit 1
    fi

    if [[ -n "$latest_image" ]]; then
        echo "找到最新镜像 $latest_image，正在拉取..."
        docker pull "$latest_image" && echo "镜像拉取成功。"
    else
        echo "未找到任何远程镜像，从本地文件导入镜像。"
        docker load < ~/tm_dlp.tar
    fi

    docker run -d --name tm_dlp --privileged --network=host -v /dev/hugepages:/dev/hugepages -v /dev/shm/session_json/:/dev/shm/session_json/ "$latest_image" /opt/TM_DLP/dlp_start.sh
    echo "容器已创建并启动。"
    docker update --restart=always tm_dlp
else
    container_running=$(docker ps --format '{{.Names}}' | grep -w "tm_dlp")
    if [ -z "$container_running" ]; then
        echo "容器tm_dlp已存在但未运行，正在启动容器。"
        docker start tm_dlp
    else
        echo "容器tm_dlp已存在且正在运行，无需进行操作。"
    fi
fi