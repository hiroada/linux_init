#!/bin/bash

# 定义目录路径
DIR="/dev/shm/session_json"

# 检查目录是否存在
if [ ! -d "$DIR" ]; then
    # 如果目录不存在，创建目录
    mkdir "$DIR"
    
    # 检查目录是否创建成功
    if [ $? -eq 0 ]; then
        echo "Directory $DIR has been created successfully."
    else
        echo "Failed to create directory $DIR." >&2
        exit 1
    fi
else
    echo "Directory $DIR already exists."
fi

# 设置权限，使得所有用户都可以访问
chmod 777 "$DIR"

# 通知用户权限已设置
echo "Access permissions for $DIR have been set for all users."