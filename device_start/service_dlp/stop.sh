#!/bin/bash

# 获取本脚本所在目录
SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

# 切换到本脚本所在目录
cd "$SCRIPT_DIR"

# 定义一个函数来运行目录下的所有脚本
run_all_scripts_in_directory() {
    for script in "$1"/*; do
        if [ -x "$script" ] && [ ! -d "$script" ]; then
            "$script"
        fi
    done
}

# 运行当前目录下 dlp_common 目录下的所有脚本
run_all_scripts_in_directory ./ndlp_stop