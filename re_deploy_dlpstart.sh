#!/bin/sh
set -e  # 告诉bash，如果有任何命令失败，立即退出
./make_executable.sh


# 初始化启动脚本
if grep -qa docker /proc/1/cgroup; then
    echo "Docker 容器内不安装初始化脚本"
else
    # 复制 service_dlp 目录到根目录 /
    sudo cp ./device_start/service_dlp / -rf
    # 更改 /service_dlp 目录及其内容的权限为 777
    sudo chmod -R 777 /service_dlp
    # 复制 .service 文件到 systemd 目录
    sudo cp /service_dlp/*.service /etc/systemd/system/
    # 重新加载 systemd 配置
    sudo systemctl daemon-reload
    # 启用 dlp.service 使其开机自启动
    sudo systemctl enable dlp.service
    # 启动 dlp.service
    sudo systemctl start dlp.service
fi



