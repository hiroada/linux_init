#!/bin/bash

# 获取当前目录下所有.sh文件
sh_files=$(find . -type f -name "*.sh")

# 遍历文件列表并更改权限
for file in $sh_files; do
    chmod a+x "$file"
done

echo "All .sh files in the current directory are now executable by everyone."