#!/bin/bash

# 检查是否提供了代理参数
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <proxy>"
    echo "Example: $0 10.22.96.29:8080"
    exit 1
fi

# 读取输入的代理地址
PROXY=$1

# 定义所需的环境变量
http_proxy_value="export HTTP_PROXY=\"http://$PROXY\""
https_proxy_value="export HTTPS_PROXY=\"https://$PROXY\""

# 检查 /etc/profile 中是否已设置 HTTP_PROXY
if ! grep -qF "$http_proxy_value" /etc/profile; then
    echo "$http_proxy_value" | sudo tee -a /etc/profile > /dev/null
    echo "HTTP_PROXY added to /etc/profile"
else
    echo "HTTP_PROXY already set in /etc/profile"
fi

# 检查 /etc/profile 中是否已设置 HTTPS_PROXY
if ! grep -qF "$https_proxy_value" /etc/profile; then
    echo "$https_proxy_value" | sudo tee -a /etc/profile > /dev/null
    echo "HTTPS_PROXY added to /etc/profile"
else
    echo "HTTPS_PROXY already set in /etc/profile"
fi

# 配置 Yum 代理 ############################################
proxy_setting="proxy=http://$PROXY"

# 检查 /etc/yum.conf 中是否已设置 proxy
if ! grep -q "^$proxy_setting" /etc/yum.conf; then
    echo "Configuring Yum to use the proxy..."
    echo "$proxy_setting" | sudo tee -a /etc/yum.conf > /dev/null
    echo "Proxy setting added to /etc/yum.conf"
else
    echo "Proxy setting already exists in /etc/yum.conf"
fi

# 配置系统级 Git
yum install git -y
echo "Configuring system-wide Git to use the proxy..."
sudo git config --system http.proxy "http://$PROXY"
sudo git config --system https.proxy "http://$PROXY"

# 提示完成
echo "Proxy configuration completed."
echo "Proxy set to http://$PROXY"