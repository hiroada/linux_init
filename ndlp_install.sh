#!/bin/sh
set -e  # 告诉bash，如果有任何命令失败，立即退出

conf_command="./conf_ndlp"
# 定义函数来运行菜单
run_menu() {
    local -a options=("${!1}")
    local -a params=("${!2}")
    while true; do
        # 显示选项
        echo "请选择一个选项:"
        for i in "${!options[@]}"; do
            echo "[$((i+1))] ${options[$i]}"
        done

        # 读取用户输入
        read -p "请输入选项编号 (按 'q' 退出): " choice

        # 处理用户输入
        if [[ $choice =~ ^[1-9][0-9]*$ ]]; then
          local -i index=$((choice - 1))
          if [[ $index -ge 0 && $index -lt ${#options[@]} ]]; then
            # 将对应的参数追加到配置参数字符串
            conf_command+=" ${params[$index]}"
            echo "选择了 ${options[$index]}"
            break
          else
            echo "无效的选项，请重新选择！"
          fi
        elif [[ $choice == "q" || $choice == "Q" ]]; then
          echo "退出"
          exit 1
        else
          echo "无效的选项，请重新选择！"
        fi
    done
}
# 示例数据
# echo "请选择DLP安装方式"
# options=("旁路监测" "串行监测" "串行阻断")
# params=("-mn=1" "-mn=0 -ub=1" "-mn=0 -ub=0")
# run_menu options[@] params[@]

# options=("多进程" "单进程")
# params=("-muti=1" "-muti=0")
# run_menu options[@] params[@]


rm -rf /opt/TM_DLP/dlp_packet
rm -rf /opt/TM_DLP/dlp_audit
rm -rf /opt/TM_DLP/dlp_manage

mkdir -p /opt/TM_DLP/
mkdir -p /opt/TM_DLP/dlp_manage
mkdir -p /opt/TM_DLP/dlp_manage/log

\cp ../ndlp /opt/TM_DLP/dlp_packet -R
\cp ../audit /opt/TM_DLP/dlp_audit -R
\cp ../AllInOne.jar /opt/TM_DLP/dlp_manage/AllInOne.jar
\cp ../get.py /opt/TM_DLP/dlp_manage/get.py
\cp ../protect.sh /opt/TM_DLP/dlp_manage/protect.sh
\cp ../shutdown.sh /opt/TM_DLP/dlp_manage/shutdown.sh

cd /opt/TM_DLP/dlp_manage
rm protect.sh -rf
cat <<EOF > protect.sh
#!/bin/bash
nohup java -Dlogging.config=classpath:logback-protect.xml -jar AllInOne.jar --spring.profiles.active=protect,commonProtect > /dev/null 2>&1 &
EOF
chmod 777 protect.sh

cd /opt/TM_DLP/dlp_packet
rm start.sh -rf
cat <<EOF > start.sh
#!/bin/bash
#$conf_command
nohup ./ndlp ./conf/conf_flows.json  > /dev/null 2>&1 &
EOF
chmod 777 start.sh

rm start.py -rf
cat <<EOF > start.py
#!/usr/bin/env python3
#coding:utf-8

import os
import subprocess

print("启动packet")
os.chdir('/opt/TM_DLP/dlp_packet/')
#subprocess.Popen(['sh', '-c', '$conf_command'], stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT)
subprocess.Popen(['./ndlp', './conf/conf_flows.json'], stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT)
EOF
chmod 777 start.py

cd /opt/TM_DLP/dlp_audit
rm start.sh -rf

if [ -z "$1" ]; then
  num_processes=4
else
  # 确保参数是一个数字
  if [[ $1 =~ ^[0-9]+$ ]]; then
    num_processes=$1
  else
    echo "Error: Argument must be an integer. set num 2."
    num_processes=4
  fi
fi

cat <<EOF > start.sh
#!/bin/bash
# ndlp 进程的命令行参数特征字符串

max_attempts=5
# 循环检查进程是否运行
for ((attempt=1; attempt<=max_attempts; attempt++)); do
    current_pids=\$(pgrep -f "./ndlp ./conf/conf_flows.json")
    if [[ -n \$current_pids ]]; then
        echo "ndlp 进程已启动，可以执行下一步。"
        sleep 10

        # 再次检查 ndlp 进程的 PID
        new_pids=\$(pgrep -f "./ndlp ./conf/conf_flows.json")
        
        # 比较两次获取的 PID 是否相同
        if [[ \$current_pids == \$new_pids ]]; then
            echo "ndlp 进程的 PID 未发生变化，可以继续执行下一步。"
            # 这里可以添加你的下一步操作代码
            ./conf_audit
            
            # 循环启动指定数量的进程
            for ((i=1; i<=$num_processes; i++))
            do
                nohup ./audit ./conf/conf_audit_micro.json > /dev/null 2>&1 &
            done
            
            break
        else
            echo "ndlp 进程的 PID 发生变化，重新开始循环。"
        fi
    else
        echo "ndlp 进程尚未全部启动，等待 2 秒后重试。"
        sleep 2
    fi
done
EOF

chmod 777 start.sh