#!/bin/sh

# 创建工作目录
mkdir -p /work/go/GOPATH

# 下载并安装 Go
GO_VERSION="1.22.0"
GO_TAR="go${GO_VERSION}.linux-amd64.tar.gz"
GO_URL="https://studygolang.com/dl/golang/${GO_TAR}"

# 下载 Go 安装包
curl -L -o ${GO_TAR} ${GO_URL}

# 删除旧的 Go 版本并解压新的 Go 版本
rm -rf /usr/local/go
tar -C /usr/local -xzf ${GO_TAR}

# 设置工作目录权限
chmod -R 777 /work

# 配置环境变量
cat <<EOF >> /etc/profile.d/go_env.sh
export GOROOT=/usr/local/go
export GOPATH=/work/go/GOPATH
export PATH=\$PATH:\$GOROOT/bin:\$GOPATH/bin
export GO111MODULE=on
export GOPROXY=https://goproxy.cn,direct
export GOSUMDB=off
EOF

# 加载新的环境变量
source /etc/profile.d/go_env.sh

# 设置 Go 环境变量
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct

# 安装 delve 调试工具
go install github.com/go-delve/delve/cmd/dlv@latest

# 清理临时文件
rm -f ${GO_TAR}