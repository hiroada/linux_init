#!/bin/sh
# git submodule update --init --recursive 
set -e  # 告诉bash，如果有任何命令失败，立即退出
./make_executable.sh
./stop_selinux.sh
./stop_aslr.sh

# 定义要检查的环境变量及其值 AllInOne
yum install -y jdk-1.8
secretKey="export secretKey=64EC7C763AB7BF64E2D75FF83A319918"  
zipKey="export zipKey=05d986b1141227cb20d46d0b5687c4f5"
# 检查 /etc/profile 是否已包含这些环境变量  
if ! grep -qxF "$secretKey" /etc/profile && ! grep -qxF "$zipKey" /etc/profile; then  
    echo -e "$secretKey\n$zipKey" | sudo tee -a /etc/profile > /dev/null  
    # 如果使用重定向（不推荐用于脚本，因为它不会处理错误），可以这样做：  
    # echo -e "$secretKey\n$zipKey" | sudo sh -c 'cat >> /etc/profile'  
    echo "已向 /etc/profile 添加了 $secretKey 和 $zipKey"  
else  
    echo "/etc/profile 已包含 $secretKey 和 $zipKey"  
fi

# # 检查当前目录下是否存在 downloads 文件夹
# if [ ! -d "downloads" ]; then
#     echo "downloads 文件夹不存在，执行 git submodule update --init --recursive"
#     git submodule update --init --recursive
# else
#     echo "downloads 文件夹已存在，不需要更新子模块。"
# fi
# # 获取当前目录的绝对路径
current_dir=$(pwd)

# 构建下载目录的路径
downloads_dir="${current_dir}/downloads"

# 设置环境变量
export watm_downloads="${downloads_dir}"

# 输出结果以验证
echo "watm_downloads 环境变量已设置为: ${watm_downloads}"

# 获取主板信息
device_name=$(sudo dmidecode -t baseboard | grep "Product Name" | cut -d: -f2 | tr -d ' ')
cpu_model=$(lscpu | grep "Model name" | awk -F: '{print $2}' | sed 's/^ *//g' | head -n 1)
# 定义一个函数来运行目录下的所有脚本
run_all_scripts_in_directory() {
    # 保存当前目录
    local current_dir=$(pwd)
    # 切换到目标目录
    cd "$1" || return
    # 在目标目录下执行所有脚本
    for script in ./*; do
        if [ -x "$script" ] && [ ! -d "$script" ]; then
            ./"$script"
        fi
    done
    # 切换回原始目录
    cd "$current_dir" || return
}

# 初始化宝德设备
if [ "$device_name" = "SC612DI-16F" ]; then

    # 运行当前目录下 linux_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/linux_common

    # 运行当前目录下 docker_install 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/docker_install

    # 运行当前目录下 dlp_database 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/dlp_database

    # 运行当前目录下 ndlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/ndlp_common

    # 运行当前目录下 ndlp_baode 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/ndlp_baode

    # 最后运行当前目录下的 update_grub_config.sh 脚本
    ./device_init/update_grub_config.sh
fi

# 初始化曙光128核设备
if [ "$device_name" = "62DB32" ]; then

    # 运行当前目录下 linux_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/linux_common
    
    # 运行当前目录下 docker_install 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/docker_install
    
    # 检查mysqld是否存在  
    if command -v mysqld &> /dev/null  
    then  
        echo "mysql 已经安装。"  
    else  
        echo "安装mysql...."  
        # 运行当前目录下 dlp_database 目录下的所有脚本
        run_all_scripts_in_directory ./device_init/dlp_database
    fi
    
    # 运行当前目录下 ndlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/ndlp_common
    # 曙光设备128核项目46台设备
    if [ "$cpu_model" = "Hygon C86 7390 32-core Processor" ]; then
        run_all_scripts_in_directory ./device_init/ndlp_sg_128_7390
    fi
    # 曙光设备128核
    if [ "$cpu_model" = "Hygon C86 7381 32-core Processor" ]; then
        run_all_scripts_in_directory ./device_init/ndlp_sg_128_7381
    fi
    # 最后运行当前目录下的 update_grub_config.sh 脚本
    ./device_init/update_grub_config.sh
fi

# 初始化曙光128核设备
if [ "$device_name" = "BC13MBSBH" ]; then

    # 运行当前目录下 linux_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/linux_common
    
    # 运行当前目录下 docker_install 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/docker_install
    
    # 检查mysqld是否存在  
    if command -v mysqld &> /dev/null  
    then  
        echo "mysql 已经安装。"  
    else  
        echo "安装mysql...."  
        # 运行当前目录下 dlp_database 目录下的所有脚本
        run_all_scripts_in_directory ./device_init/dlp_database
    fi
    
    # 运行当前目录下 ndlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/ndlp_common
    # 曙光设备128核项目46台设备
    if [ "$cpu_model" = "Intel(R) Xeon(R) Silver 4314 CPU @ 2.40GHz" ]; then
        run_all_scripts_in_directory ./device_init/ndlp_xh_64_4314
    fi
    # 曙光设备128核
    if [ "$cpu_model" = "Intel(R) Xeon(R) Platinum 8358P CPU @ 2.60GHz" ]; then
        run_all_scripts_in_directory ./device_init/ndlp_xh_128_8358P
    fi
    # 最后运行当前目录下的 update_grub_config.sh 脚本
    ./device_init/update_grub_config.sh
fi

if [ "$device_name" = "52DB16" ]; then

    # 运行当前目录下 linux_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/linux_common

    # 运行当前目录下 docker_install 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/docker_install

    # 运行当前目录下 dlp_database 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/dlp_database

    # 运行当前目录下 ndlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/ndlp_common

    # 运行当前目录下 ndlp_baode 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/ndlp_sg_64

    # 最后运行当前目录下的 update_grub_config.sh 脚本
    ./device_init/update_grub_config.sh
fi

if [ "$device_name" = "X11DPH-i" ]; then
    # 运行当前目录下 linux_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/linux_common

    # 运行当前目录下 ndlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/ndlp_common

    # 运行当前目录下 ndlp_baode 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/ndlp_arp690_96

    # 最后运行当前目录下的 update_grub_config.sh 脚本
    ./device_init/update_grub_config.sh
fi

# 初始化docker容器网络探针
if grep -qa docker /proc/1/cgroup; then
    # 运行当前目录下 linux_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/linux_common
    # 运行当前目录下 ndlp_common 目录下的所有脚本
    run_all_scripts_in_directory ./device_init/ndlp_common
fi

# 初始化启动脚本
if grep -qa docker /proc/1/cgroup; then
    echo "Docker 容器内不安装初始化脚本"
else
    # 复制 service_dlp 目录到根目录 /
    sudo cp ./device_start/service_dlp / -rf
    # 更改 /service_dlp 目录及其内容的权限为 777
    sudo chmod -R 777 /service_dlp
    # 复制 .service 文件到 systemd 目录
    sudo cp /service_dlp/*.service /etc/systemd/system/
    # 重新加载 systemd 配置
    sudo systemctl daemon-reload
    # 启用 dlp.service 使其开机自启动
    sudo systemctl enable dlp.service
    # 启动 dlp.service
    sudo systemctl start dlp.service
    # reboot
fi



