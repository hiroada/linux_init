#!/bin/bash

# 检查是否提供了参数
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <PCI bus ID>"
    exit 1
fi

pci_bus_id=$1

# 检查输入的 PCI bus ID 是否存在
if [ ! -d "/sys/bus/pci/devices/$pci_bus_id" ]; then
    echo "No device found at PCI bus ID $pci_bus_id"
    exit 1
fi

# 获取 NUMA 节点
numa_node=$(cat /sys/bus/pci/devices/$pci_bus_id/numa_node)
if [ "$numa_node" == "-1" ]; then
    echo "NUMA node information not available for this device."
    exit 1
fi

echo "NUMA Node: $numa_node"

# 列出这个 NUMA 节点的所有 CPU
cpus=$(numactl --hardware | grep "node $numa_node cpus:" | cut -d ':' -f2)
echo "CPUs on NUMA Node $numa_node: $cpus"

# 检查 NUMA 节点上的内存信息
memory_info=$(numactl --hardware | grep "node $numa_node size:" | cut -d ':' -f2 | sed 's/ MB//')
if [ "$memory_info" -gt 0 ]; then
    echo "Memory on NUMA Node $numa_node: $memory_info MB"
else
    echo "No memory available on NUMA Node $numa_node."
fi