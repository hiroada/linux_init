#!/bin/bash

# 检查是否提供了足够的参数
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <remote_host>"
    exit 1
fi

# 从命令行参数获取远程主机地址
REMOTE_HOST=$1

# 创建所需的目录

# 定义远端路径的基础部分
BASE_PATH="/var/lib/openproject/"
REMOTE_BASE_PATH="${REMOTE_HOST}:${BASE_PATH}"



# 从远端复制 pgdata 目录到本地
echo "Copying pgdata from remote to local..."
scp -r "${REMOTE_BASE_PATH}" "${BASE_PATH}"


echo "Copy operations completed successfully."