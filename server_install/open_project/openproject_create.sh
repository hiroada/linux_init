#!/bin/bash

# 检查是否提供了足够的参数
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <ip_address> <port>"
    exit 1
fi

# 从命令行参数获取IP地址和端口号
IP_ADDRESS=$1
PORT=$2

# 创建所需的目录
mkdir -p /var/lib/openproject/{pgdata,assets}

# 运行Docker容器
docker run -d -p ${PORT}:80 --name openproject \
  -e OPENPROJECT_SECRET_KEY_BASE=secret \
  -e OPENPROJECT_HOST__NAME=${IP_ADDRESS}:${PORT} \
  -e OPENPROJECT_HTTPS=false \
  -v /var/lib/openproject/pgdata:/var/openproject/pgdata \
  -v /var/lib/openproject/assets:/var/openproject/assets \
  openproject/openproject:14

echo "OpenProject container started on ${IP_ADDRESS}:${PORT}"