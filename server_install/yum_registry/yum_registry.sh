#!/bin/bash

# 设置脚本遇到错误时立即退出
set -e

docker build -t yum_registry .
docker run -d --network host --name yum_registry yum_registry
docker update --restart always yum_registry