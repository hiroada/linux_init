#!/bin/bash

# 设置脚本遇到错误时立即退出
set -e

# Source the /etc/profile to load environment variables
source /etc/profile

# Function to create or update Docker proxy configuration
setup_proxy() {
    # Create Docker systemd drop-in directory if it doesn't exist
    sudo mkdir -p /etc/systemd/system/docker.service.d

    # Create or overwrite the http-proxy.conf file with the proxy settings
    sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf > /dev/null <<EOF
[Service]
Environment="HTTP_PROXY=$HTTP_PROXY"
Environment="HTTPS_PROXY=$HTTPS_PROXY"
Environment="NO_PROXY=localhost,127.0.0.1"
EOF

    # Reload systemd to apply changes
    sudo systemctl daemon-reload

    # Restart Docker service to apply proxy settings
    sudo systemctl restart docker

    echo "Docker proxy settings updated successfully with HTTP_PROXY and HTTPS_PROXY."
}

# Check if HTTP_PROXY and HTTPS_PROXY are set in the environment
if [ ! -z "$HTTP_PROXY" ] && [ ! -z "$HTTPS_PROXY" ]; then
    setup_proxy
else
    echo "HTTP_PROXY and HTTPS_PROXY are not set. No proxy configuration will be applied to Docker."
fi

# 函数：检查 Docker Registry 容器是否运行
check_registry_running() {
    if docker ps | grep -q 'registry'; then
        echo "Docker Registry is already running."
        exit 0
    fi
}

# 函数：检查 Docker Registry 镜像是否存在
check_registry_image() {
    if ! docker images | grep -q 'registry.*2'; then
        echo "Docker Registry image not found. Pulling image..."
        docker pull registry:2
    else
        echo "Docker Registry image already exists."
    fi
}

# 函数：创建持久化数据目录并设置权限
create_persistent_directory() {
    if [ ! -d "/opt/docker_registry_data" ]; then
        echo "Creating directory /opt/docker_registry_data for Docker Registry data..."
        mkdir -p /opt/docker_registry_data
        echo "Setting permissions to 777 for /opt/docker_registry_data..."
        chmod 777 /opt/docker_registry_data
    else
        echo "/opt/docker_registry_data already exists. Ensuring permissions are set to 777..."
        chmod 777 /opt/docker_registry_data
    fi
}

# 函数：启动 Docker Registry 容器
start_docker_registry() {
    echo "Starting Docker Registry..."
    docker run -d -p 5000:5000 --restart=always --name registry \
        -v /opt/docker_registry_data:/var/lib/registry \
        registry:2
    echo "Docker Registry started with persistent storage at /opt/docker_registry_data and listening on port 5000."
}

# 主执行流程
check_registry_running
check_registry_image
create_persistent_directory
start_docker_registry