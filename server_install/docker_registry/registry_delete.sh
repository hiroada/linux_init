#!/bin/bash

# 检查参数数量
if [ "$#" -ne 3 ]; then
    echo "用法: $0 <registry_host:port> <repository> <tag>"
    echo "./registry_delete.sh localhost:5000 dlp_img_mini_360cb6e latest"
    echo "没有tag 标签需要写为 latest"
    exit 1
fi

# 从命令行参数读取值
REGISTRY_HOST=$1
REPOSITORY=$2
TAG=$3

# 检查 jq 是否安装
if ! command -v jq &> /dev/null
then
    echo "jq could not be found, please install jq to use this script."
    echo "On Ubuntu: sudo apt-get install jq"
    echo "On CentOS: sudo yum install jq"
    echo "On MacOS: brew install jq"
    exit 1
fi

# 获取镜像的 digest
MANIFEST_RESPONSE=$(curl -s -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
    "http://$REGISTRY_HOST/v2/$REPOSITORY/manifests/$TAG" -i)

DIGEST=$(echo "$MANIFEST_RESPONSE" | grep -Fi 'Docker-Content-Digest' | awk '{print $2}' | tr -d $'\r')

if [ -z "$DIGEST" ]; then
    echo "无法获取镜像的 Digest"
    # 为了调试，显示完整的 HTTP 响应
    echo "完整的 HTTP 响应:"
    echo "$MANIFEST_RESPONSE"
    exit 1
fi

# 输出获取的 Digest
echo "获取到的 Digest: $DIGEST"

# 删除镜像
RESPONSE=$(curl -s -o /dev/null -w "%{http_code}" -X DELETE \
    "http://$REGISTRY_HOST/v2/$REPOSITORY/manifests/$DIGEST")

if [ "$RESPONSE" == "202" ]; then
    echo "镜像已成功删除"
else
    echo "删除失败，HTTP 状态码: $RESPONSE"
fi