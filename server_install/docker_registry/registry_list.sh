#!/bin/bash

# 检查参数数量
if [ "$#" -ne 1 ]; then
    echo "用法: $0 <registry-host>"
    echo "示例: $0 localhost:5000"
    exit 1
fi

# 从命令行参数读取 Docker Registry 的主机名
REGISTRY_HOST=$1

# 获取所有仓库列表的 JSON 数据
REPOSITORIES_JSON=$(curl -s "http://$REGISTRY_HOST/v2/_catalog")
REPOSITORIES=$(echo "$REPOSITORIES_JSON" | jq -r '.repositories[]')

echo "有效的仓库及其标签列表:"

# 遍历每个仓库
for REPO in $REPOSITORIES; do
    # 获取该仓库的所有标签
    TAGS_JSON=$(curl -s "http://$REGISTRY_HOST/v2/$REPO/tags/list")
    TAGS=$(echo "$TAGS_JSON" | jq -r '.tags[]?')

    # 检查是否有标签存在
    if [ -n "$TAGS" ]; then
        # 打印仓库名和标签
        for TAG in $(echo "$TAGS_JSON" | jq -r '.tags[]'); do
            echo "$REPO:$TAG"
        done
    fi
done